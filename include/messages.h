#pragma once

#include "io.h"

const char * read_error_messages[] = {
        [READ_INVALID_SIGNATURE] = "Failed to read file: Invalid file signature\n",
        [READ_INVALID_BITS] = "Failed to read file: File header does not match file contents\n",
        [READ_INVALID_HEADER] = "Failed to read file: Invalid file header\n"
};

const char * write_error_messages[] = {
        [WRITE_ERROR] = "Failed to write to file\n"
};

const char * open_error_messages[] = {
        [OPEN_ERROR] = "Failed to open file\n"
};

const char * close_error_messages[] = {
        [CLOSE_ERROR] = "Failed to close file\n"
};
