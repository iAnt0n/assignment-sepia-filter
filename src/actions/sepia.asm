global apply_sepia_simd_i

section .data

align 16
sepia_coef_1_1: dd 0.131, 0.168, 0.189, 0.131
align 16
sepia_coef_1_2: dd 0.543, 0.686, 0.769, 0.543
align 16
sepia_coef_1_3: dd 0.272, 0.349, 0.393, 0.272
align 16
sepia_coef_2_1: dd 0.168, 0.189, 0.131, 0.168
align 16
sepia_coef_2_2: dd 0.686, 0.769, 0.543, 0.686
align 16
sepia_coef_2_3: dd 0.349, 0.393, 0.272, 0.349
align 16
sepia_coef_3_1: dd 0.189, 0.131, 0.168, 0.189
align 16
sepia_coef_3_2: dd 0.769, 0.543, 0.686, 0.769
align 16
sepia_coef_3_3: dd 0.393, 0.272, 0.349, 0.393
align 16
max: dd 255, 255, 255, 255

section .text
; b1g1r1b2g2r2... pointer- rdi, res - rsi
apply_sepia_simd_i:
    movdqu xmm0, [rdi]
    shufps xmm0, xmm0, 0b11000000

    movdqu xmm1, [rdi+4]
    shufps xmm1, xmm1, 0b11000000

    movdqu xmm2, [rdi+8]
    shufps xmm2, xmm2, 0b11000000

    mulps xmm0, [sepia_coef_1_1]
    mulps xmm1, [sepia_coef_1_2]
    mulps xmm2, [sepia_coef_1_3]

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvtps2dq xmm0, xmm0
    pminsd xmm0, [max]

    pextrb [rsi], xmm0, 0
    pextrb [rsi + 1], xmm0, 4
    pextrb [rsi + 2], xmm0, 8
    pextrb [rsi + 3], xmm0, 12

    ; ==========
    add rdi, 12
    add rsi, 4

    movdqu xmm0, [rdi]
    shufps xmm0, xmm0, 0b11110000

    movdqu xmm1, [rdi+4]
    shufps xmm1, xmm1, 0b11110000

    movdqu xmm2, [rdi+8]
    shufps xmm2, xmm2, 0b11110000

    mulps xmm0, [sepia_coef_2_1]
    mulps xmm1, [sepia_coef_2_2]
    mulps xmm2, [sepia_coef_2_3]

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvtps2dq xmm0, xmm0
    pminsd xmm0, [max]

    pextrb [rsi], xmm0, 0
    pextrb [rsi + 1], xmm0, 4
    pextrb [rsi + 2], xmm0, 8
    pextrb [rsi + 3], xmm0, 12

    ; ==========
    add rdi, 12
    add rsi, 4

    movdqu xmm0, [rdi]
    shufps xmm0, xmm0, 0b11111100

    movdqu xmm1, [rdi+4]
    shufps xmm1, xmm1, 0b11111100

    movdqu xmm2, [rdi+8]
    shufps xmm2, xmm2, 0b11111100

    mulps xmm0, [sepia_coef_3_1]
    mulps xmm1, [sepia_coef_3_2]
    mulps xmm2, [sepia_coef_3_3]

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvtps2dq xmm0, xmm0
    pminsd xmm0, [max]

    pextrb [rsi], xmm0, 0
    pextrb [rsi + 1], xmm0, 4
    pextrb [rsi + 2], xmm0, 8
    pextrb [rsi + 3], xmm0, 12

    ret
