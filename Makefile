CFLAGS:=--std=c18 -Wall -pedantic -Iinclude/ -Iinclude/actions/ -Iinclude/formats/ -ggdb -Wextra -Werror -DDEBUG
ASFLAGS:=-felf64 -g
LDFLAGS:=-no-pie

CC=gcc
SOURCEDIR = src
BUILDDIR = build

SRC:=$(wildcard $(SOURCEDIR)/*.c $(SOURCEDIR)/actions/*.c $(SOURCEDIR)/formats/*.c)
OBJS:=$(patsubst $(SOURCEDIR)/%.c, $(BUILDDIR)/%.o, $(SRC))

SRC+=$(SOURCEDIR)/actions/sepia.asm
OBJS+=$(BUILDDIR)/sepia_asm.o

all: $(OBJS)
	$(CC) $(LDFLAGS) $^ -o $(BUILDDIR)/img_edit

dir:
	@mkdir -p $(BUILDDIR)/actions $(BUILDDIR)/formats

$(BUILDDIR)/sepia_asm.o : $(SOURCEDIR)/actions/sepia.asm
	nasm $(ASFLAGS) $< -o $@

$(BUILDDIR)/%.o: $(SOURCEDIR)/%.c | dir
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
