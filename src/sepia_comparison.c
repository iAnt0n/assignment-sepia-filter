#include "sepia_comparison.h"

#include "actions/sepia.h"

#include <sys/resource.h>
#include <sys/time.h>
#include <stdio.h>

int compare_sepia_time(const struct image * img) {
    uint64_t sum_naive = 0, sum_simd = 0;

    for (int i = 0; i < 10; ++i) {
        struct rusage r;
        struct timeval start, naive_end, simd_end;

        getrusage(RUSAGE_SELF, &r );
        start = r.ru_utime;

        struct image sepia_res = apply_sepia(img);
        getrusage(RUSAGE_SELF, &r );
        naive_end = r.ru_utime;

        struct image sepia_res_simd = apply_sepia_simd(img);
        getrusage(RUSAGE_SELF, &r );
        simd_end = r.ru_utime;

        sum_naive += ((naive_end.tv_sec - start.tv_sec) * 1000000L) + naive_end.tv_usec - start.tv_usec;
        sum_simd += ((simd_end.tv_sec - naive_end.tv_sec) * 1000000L) + simd_end.tv_usec - naive_end.tv_usec;

        image_destroy(&sepia_res);
        image_destroy(&sepia_res_simd);
    }

    printf("Naive C implementation, average time elapsed in microseconds  : %ld\n", sum_naive / 10);
    printf("SIMD implementation, average time elapsed in microseconds     : %ld\n", sum_simd / 10);

    return 0;
}
