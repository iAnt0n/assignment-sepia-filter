#pragma once

#include "../image.h"

struct image apply_sepia(const struct image * img);
struct image apply_sepia_simd(const struct image * img);
