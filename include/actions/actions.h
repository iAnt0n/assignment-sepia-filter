#pragma once

#include "black_and_white.h"
#include "rotate.h"
#include "sepia.h"

typedef struct image do_action(const struct image *);

enum action {
    ROTATE = 0,
    BW,
    SEPIA,
    SEPIA_SIMD
    //More possible actions
};

do_action* const actions[] = {
        [ROTATE] = rotate_90,
        [BW] = to_bw,
        [SEPIA] = apply_sepia,
        [SEPIA_SIMD] = apply_sepia_simd
};

const struct {
    enum action action_enum;
    const char* action_str;
} actions_array[] = {
        {ROTATE, "rotate"},
        {BW, "bw"},
        {SEPIA, "sepia"},
        {SEPIA_SIMD, "sepia_simd"},
};
